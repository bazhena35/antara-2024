import model.Kotik;

public class Application {

  public static void main(String[] args) {
    Kotik cat1 = new Kotik("Вася",  5, "Мяаааа", 2);
    Kotik cat2 = new Kotik();
    cat2.setKotik("Барсик", 2,  "Муррр", 3);
    System.out.println(cat1.getMeow().equals(cat2.getMeow()));
    //cat2.liveAnotherDay();
    cat1.liveAnotherDay();
    System.out.println("Имя кота: " + cat1.getName());
    System.out.println("Вес кота: " + cat1.getWeight() + " кг");
    System.out.println("Создано объектов Kotik: " + Kotik.getCount());
  }
}
