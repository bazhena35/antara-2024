package model;

import java.util.Random;

public class Kotik {

  private static int count;
  private String name;
  private int weight;
  private String meow;
  private int satiety = 3;

  {
    count++;
  }

  public Kotik() {

  }

  public Kotik(String name, int weight, String meow, int satiety) {
    setKotik(name, weight, meow, satiety);
  }

  public static int getCount() {
    return count;
  }

  public void setKotik(String name,  int weight, String meow, int satiety) {
    setName(name);
    setWeight(weight);
    setMeow(meow);
    setSatiety(satiety);
  }

  public String getName() {
    return name;
  }

  private void setName(String name) {
    this.name = name;
  }

  public int getWeight() {
    return weight;
  }

  private void setWeight(int weight) {
    this.weight = weight;
  }

  public String getMeow() {
    return meow;
  }

  private void setMeow(String meow) {
    this.meow = meow;
  }

  private void setSatiety(int satiety) {
    this.satiety = satiety;
  }


  public boolean play() {
    if (isHungry()) {
      System.out.println(name + " играет");
      satiety--;
      return true;
    }
    return false;
  }

  public boolean sleep() {
    if (isHungry()) {
      System.out.println(name + " спит");
      satiety--;
      return true;
    }
    return false;
  }

  public boolean chaseMouse() {
    if (isHungry()) {
      System.out.println(name + " ловит мышей");
      satiety--;
      return true;
    }
    return false;
  }

  public boolean walk() {
    if (isHungry()) {
      System.out.println(name + " гуляет");
      satiety--;
      return true;
    }
    return false;
  }

  public boolean eat(int satiety) {
    this.satiety += satiety;
    return true;
  }

  public boolean eat() {
    return eat(2, "мясо");
  }

  public boolean eat(int satiety, String food) {
    System.out.println(name + " ест " + food);
    this.satiety += satiety;
    return true;
  }

  private boolean isHungry() {
    if (this.satiety <= 0) {
      System.out.print("  >> " + name + " хочет есть!");
      eat();
      return false;
    }
    return true;
  }

  public void liveAnotherDay() {
    for (int i = 1; i < 25; i++) {
      System.out.print(i + " ");
      switch (new Random().nextInt(4) + 1) {
        case 1:
          play();
          break;
        case 2:
          sleep();
          break;
        case 3:
          walk();
          break;
        case 4:
          chaseMouse();
          break;
      }
    }
  }
}
